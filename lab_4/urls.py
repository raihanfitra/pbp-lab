from django.urls import path
from .views import index
from .views import add_tugas

urlpatterns = [
    path('', index, name = 'index'),
    path('add', add_tugas, name = 'add-tugas')
]