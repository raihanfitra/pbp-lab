from django import forms
from lab_2.models import TrackerTugas

class DateInput(forms.DateInput):
    input_type = 'date'

class TugasForm(forms.ModelForm):
    class Meta:
        model = TrackerTugas
        fields = ['deadline', 'course', 'detail']
        widgets = {
            'deadline': forms.DateInput(),
            'course': forms.TextInput(),
            'detail': forms.Textarea(attrs={'rows':2}),
        }