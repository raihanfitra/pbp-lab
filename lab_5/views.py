from django.shortcuts import render
from lab_5.models import MyWatchlist

# Create your views here.
def index(request):
    movie = MyWatchlist.objects.all()
    response = {'watchlist': movie }
    return render(request, 'lab5_index.html', response)