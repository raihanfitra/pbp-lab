from django import forms
from lab_5.models import MyWatchlist

class TugasForm(forms.ModelForm):
    model = MyWatchlist
    widgets = {
        '': forms.DateInput(),
        '': forms.TextInput(),
        '': forms.Textarea(),
    }
