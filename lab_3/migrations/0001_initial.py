# Generated by Django 3.2.7 on 2022-03-16 16:48

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MyWatchlist',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('watched', models.BooleanField(default=False)),
                ('title', models.CharField(max_length=30)),
                ('rating', models.FloatField(default=0.0)),
                ('release_date', models.DateField(verbose_name='release date')),
                ('review', models.TextField()),
            ],
        ),
    ]
