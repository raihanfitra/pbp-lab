from django.db import models

# Create your models here.

# Create MyWatchlist model that contains watched, title, rating, release_date, and review.
class MyWatchlist(models.Model):
    watched = models.BooleanField(default=False)
    title = models.CharField(max_length=30)
    rating = models.FloatField(default=0.0)
    release_date = models.DateField('release date')
    review = models.TextField()