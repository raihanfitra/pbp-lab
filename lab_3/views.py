import re
from urllib import response
from django.shortcuts import render
#import HttpResponse, serializers, dan model MyWatchlist
from django.http.response import HttpResponse
from django.core import serializers
from lab_3.models import MyWatchlist

# Create your views here.
def index(request):
    movie = MyWatchlist.objects.all()
    response = {'watchlist': movie }
    return render(request, 'index_lab3.html', response)

def xml(data):
    data = serializers.serialize('xml', MyWatchlist.objects.all())
    return HttpResponse(data, content_type = "application/xml")

def json(data):
    data = serializers.serialize('json', MyWatchlist.objects.all())
    return HttpResponse(data, content_type="application/json")