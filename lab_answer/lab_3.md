1.  XML                                                JSON
    Markup language                                    Data format
    Syntax lebih panjang                               Syntax lebih pendek
    File lebih besar, pemrosesan lebih lambat          File lebih kecil, pemrosesan lebih cepat
    Mendukung tipe data kompleks                       Hanya mendukung tipe data primitif
    Mendukung namespaces                               Tidak mendukung namespaces
    Tidak mendukung array                              Mendukung array
    Data bersifat tree structure                       Data berupa key:value

2.  HTML                                               XML
    Berfokus pada penyajian data                       Berfokus pada transfer data
    Didorong oleh format                               Didorong oleh konten
    Case Insensitive                                   Case Sensitive
    Tidak mendukung namespaces                         Mendukung namespaces
    Tidak strict tag penutup                           Stict untuk tag penutup   
