from django.db import models

# TODO Create TrackerTugas model that contains course, detail, and deadline (date) here


class TrackerTugas(models.Model):
    course = models.CharField(max_length=30)
    # TODO Implement missing attributes in TrackerTugas model
    detail = models.CharField(max_length= 200, default = "_")
    deadline = models.DateField('Deadline')
