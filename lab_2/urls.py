from django.urls import path
from .views import index, list_tugas

urlpatterns = [
    path('', index, name='index'),
    # TODO Add 'tugas' path using list_tugas Views
    path('tugas', list_tugas)
]
